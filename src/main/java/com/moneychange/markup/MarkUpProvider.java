package com.moneychange.markup;

import com.moneychange.mapper.CSVMapper;

import java.io.IOException;

public class MarkUpProvider {
    public static MarkUpTable markupTable;
    private MarkUpProvider(){

    }
    public static MarkUpTable getMarkupTable(){
        if(markupTable == null){
            markupTable = new MarkUpTable(CSVMapper.loadMarkUpTable());
        }
        return markupTable;
    }
}
