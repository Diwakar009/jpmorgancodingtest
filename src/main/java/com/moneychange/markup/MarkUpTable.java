package com.moneychange.markup;

import com.moneychange.amount.CurrencyAmount;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MarkUpTable {
    private Map<String, List<MarkUp>> table = new HashMap<String, List<MarkUp>>();

    public MarkUpTable(Map<String, List<MarkUp>> table) {
        this.table = table;
    }

    public Map<String, List<MarkUp>> getTable() {
        return table;
    }

    public MarkUp getEligibleMarkUp(String clientType, CurrencyAmount amount){
        CurrencyAmount usdAmount = amount.exchangeAmount("USD",amount.getTime());
        return
                (MarkUp) table.get(clientType)
                        .stream()
                        .filter(markUp -> {
                            int lowerComparison = usdAmount.getAmount().val().compareTo(markUp.getLowerRange());
                            int upperComparison = usdAmount.getAmount().val().compareTo(markUp.getUpperRange());
                            return (lowerComparison >= 0 && upperComparison <= 0);
                        }).findFirst().get();
    }

}
