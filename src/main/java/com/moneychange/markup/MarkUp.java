package com.moneychange.markup;

import com.moneychange.number.BasisPoint;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;


@Data
@Builder
public class MarkUp {
    private BigDecimal lowerRange;
    private BigDecimal upperRange;
    private BasisPoint basisPoint;
}
