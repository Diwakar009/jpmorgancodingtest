package com.moneychange.transaction;

import com.moneychange.number.Amount;
import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
public class Transaction {
    String baseCCY;
    String targetCCY;
    Amount baseAmount;
    String clientType;
    LocalTime time;
}
