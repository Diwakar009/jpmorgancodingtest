package com.moneychange.transaction;

import com.moneychange.number.Amount;
import com.moneychange.number.Rate;
import lombok.Builder;
import lombok.Data;

@Data
@Builder

public class TransactionResult {
    private String baseCurrency;
    private String wantedCurrency;
    private Amount amountInBaseCurrency;
    private Rate standardRate;
    private Rate finalRate;
    private Amount profitInWantedCurrency;
    private Amount profitInSGD;;
}
