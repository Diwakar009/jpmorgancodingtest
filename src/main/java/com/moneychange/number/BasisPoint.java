package com.moneychange.number;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BasisPoint {

    private BigDecimal value;
    private static final int SCALE = 0;

    public BasisPoint(String val) {
        this.value = new BigDecimal(val);
        this.value = this.value.setScale(SCALE, RoundingMode.HALF_UP);
    }

    public BasisPoint(BigDecimal val) {
        this.value = new BigDecimal(val.toString());
        this.value = this.value.setScale(SCALE, RoundingMode.HALF_UP);
    }

    public BasisPoint(Double val) {
        this.value = new BigDecimal(val);
        this.value = this.value.setScale(SCALE, RoundingMode.HALF_UP);
    }

    public BigDecimal val(){
        return value;
    }

    @Override
    public String toString() {
        return val().toString();
    }
}
