package com.moneychange.number;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Rate {

    private BigDecimal value;
    private static final int SCALE = 4;

    public Rate(String val) {
        this.value = new BigDecimal(val);
        this.value = this.value.setScale(SCALE, RoundingMode.HALF_UP);
    }

    public Rate(BigDecimal val) {
        this.value = new BigDecimal(val.toString());
        this.value = this.value.setScale(SCALE, RoundingMode.HALF_UP);
    }

    public Rate(Double val) {
        this.value = new BigDecimal(val);
        this.value = this.value.setScale(SCALE, RoundingMode.HALF_UP);
    }

    public BigDecimal val(){
        return value;
    }

    @Override
    public String toString() {
        return val().toString();
    }
}
