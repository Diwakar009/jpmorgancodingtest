package com.moneychange.rate;

import com.moneychange.number.Rate;
import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
public class ExchangeRate {
    String baseCurrency;
    String wantedCurrency;
    LocalTime time;
    Rate rate;
}
