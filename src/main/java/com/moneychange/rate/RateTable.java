package com.moneychange.rate;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

public class RateTable {

    private List<ExchangeRate> rateList = new ArrayList<>();
    private Map<String,List<ExchangeRate>> rateModelMap = new HashMap<String,List<ExchangeRate>>();
    public RateTable(List<ExchangeRate> rateList) {
        this.rateList = rateList;
        this.rateModelMap = this.rateList.stream()
                .collect(Collectors.groupingBy(rate -> rate.getBaseCurrency() + "|" + rate.getWantedCurrency()
                        , Collectors.mapping(rate -> rate, Collectors.collectingAndThen(Collectors.toList(), values -> {
                            values.sort(Comparator.comparing(ExchangeRate::getTime));
                            return values;
                        }))));

    }

      public ExchangeRate getExchangeRate(String baseCurrency, String wantedCurrency, LocalTime time){
        return rateModelMap.get(baseCurrency + "|" + wantedCurrency)
                    .stream().filter(rate -> time.isBefore(rate.getTime())).findFirst().get();
      }

      public ExchangeRate getUSDExchangeRate(String baseCurrency, LocalTime time){
        return getExchangeRate(baseCurrency,"USD",time);
      }

}
