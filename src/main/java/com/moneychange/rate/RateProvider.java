package com.moneychange.rate;

import com.moneychange.mapper.CSVMapper;

import java.util.List;
import java.util.Optional;

public class RateProvider {
    private static RateTable rateTable = null;
    private RateProvider(){}
    public static RateTable getRateTable(){
        if(rateTable == null){
            Optional<List<ExchangeRate>> rateList = CSVMapper.loadRates();
            if(rateList.isPresent()) {
                rateTable = new RateTable(rateList.get());
            }
        }
        return  rateTable;
    }
}
