package com.moneychange.mapper;

import com.moneychange.markup.MarkUp;
import com.moneychange.number.Amount;
import com.moneychange.number.BasisPoint;
import com.moneychange.number.Rate;
import com.moneychange.rate.ExchangeRate;
import com.moneychange.transaction.Transaction;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.*;


public class CSVMapper {

    public static String INPUT_TRANSACTIONS = "src/main/resources/transactions.csv";
    public static String INPUT_RATES = "src/main/resources/rates.csv";
    public static String INPUT_IND_MARKUP_RATES = "src/main/resources/indmarkuprange.csv";
    public static String INPUT_CORP_MARKUP_RATES = "src/main/resources/corpmarkuprange.csv";


    public static Optional<List<Transaction>> loadTransactions(){
        try {
            Reader reader = new FileReader(INPUT_TRANSACTIONS);
            CSVParser csvParser = CSVFormat.DEFAULT
                    .withHeader() // Specify that the CSV has a header
                    .withSkipHeaderRecord(true) // Skip the header
                    .parse(reader);

            List<Transaction> transactionList = new ArrayList<>();
            for (CSVRecord csvRecord : csvParser) {
                String[] tTime = csvRecord.get(4).split(":");
                transactionList.add(Transaction.builder().baseCCY(csvRecord.get(0))
                        .targetCCY(csvRecord.get(1))
                        .baseAmount(new Amount(csvRecord.get(2)))
                        .clientType(csvRecord.get(3))
                        .time(LocalTime.of(Integer.parseInt(tTime[0]),Integer.parseInt(tTime[1])))
                        .build());
            }
            csvParser.close();
            reader.close();
            return Optional.of(transactionList);
        }catch (IOException e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static Optional<List<ExchangeRate>> loadRates(){
        Map<String, List<ExchangeRate>> rateMap = new HashMap<String, List<ExchangeRate>>();
        try {
            Reader reader = new FileReader(INPUT_RATES);
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
            List<ExchangeRate> rateList = new ArrayList<>();
            for (CSVRecord csvRecord : csvParser) {
                String baseCurrency = csvRecord.get(0);
                String wantedCurrency = csvRecord.get(1);
                String[] time = csvRecord.get(3).split(":");
                Rate currencyRate = new Rate(csvRecord.get(2));
                ExchangeRate rate = ExchangeRate.builder().baseCurrency(baseCurrency)
                        .wantedCurrency(wantedCurrency)
                        .time(LocalTime.of(Integer.parseInt(time[0]),Integer.parseInt(time[1])))
                        .rate(currencyRate).build();
                rateList.add(rate);
            }
            csvParser.close();
            reader.close();
            return Optional.of(rateList);
         }catch (IOException e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static Map<String, List<MarkUp>> loadMarkUpTable() {
        Map<String ,List<MarkUp>> markupTable = new HashMap<String ,List<MarkUp>>();
        try {
            markupTable.put("Individual", loadIndivisualMarkupList());
            markupTable.put("Corporate", loadCorporateMarkupList());
        }catch (IOException e){
            e.printStackTrace();
        }
        return markupTable;
    }

    private static List<MarkUp> loadIndivisualMarkupList() throws IOException{
        try {
               Reader reader = new FileReader(INPUT_IND_MARKUP_RATES);
               CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
               List<MarkUp> indivisualMarkupList = new ArrayList<MarkUp>();
                for (CSVRecord csvRecord : csvParser) {
                    indivisualMarkupList.add(
                            MarkUp.builder()
                                    .lowerRange(new BigDecimal(csvRecord.get(0)))
                                    .upperRange(new BigDecimal(("~".equals(csvRecord.get(1))) ? "9999999999" :  csvRecord.get(1)))
                                    .basisPoint(new BasisPoint(csvRecord.get(2))).build());
                }
                csvParser.close();
                reader.close();
                return indivisualMarkupList;
            } catch (IOException e) {
                throw e;
            }
    }

    private static List<MarkUp> loadCorporateMarkupList() throws IOException{
        try {
            Reader reader = new FileReader(INPUT_CORP_MARKUP_RATES);
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
            List<MarkUp> corporateMarkupList = new ArrayList<MarkUp>();
            for (CSVRecord csvRecord : csvParser) {
                corporateMarkupList.add(
                        MarkUp.builder()
                                .lowerRange(new BigDecimal(csvRecord.get(0)))
                                .upperRange(new BigDecimal(("~".equals(csvRecord.get(1))) ? "9999999999" :  csvRecord.get(1)))
                                .basisPoint(new BasisPoint(csvRecord.get(2))).build());
            }
            csvParser.close();
            reader.close();
            return corporateMarkupList;
        } catch (IOException e) {
            throw e;
        }
    }



}
