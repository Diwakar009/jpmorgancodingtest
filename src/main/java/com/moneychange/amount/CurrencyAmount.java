package com.moneychange.amount;

import com.moneychange.number.Amount;
import com.moneychange.number.Rate;
import com.moneychange.rate.RateProvider;
import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
public class CurrencyAmount {
    private String currency;
    private Amount amount;
    private Rate exchangeRate = new Rate("1");
    private LocalTime time;

    public CurrencyAmount exchangeAmount(String targetCurrency, LocalTime time){
        if(targetCurrency.equals(getCurrency())){
            return this;
        }
        Rate exchangeRate = RateProvider.getRateTable().getExchangeRate(this.currency,targetCurrency,time).getRate();
        return CurrencyAmount.builder()
                .currency(targetCurrency)
                .exchangeRate(exchangeRate)
                .time(time)
                .amount(new Amount(exchangeRate.val().multiply(amount.val()))).build();

    }
}
