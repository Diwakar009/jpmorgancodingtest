package com.moneychange.service;

import com.moneychange.amount.CurrencyAmount;
import com.moneychange.markup.MarkUpProvider;
import com.moneychange.markup.MarkUpTable;
import com.moneychange.number.Amount;
import com.moneychange.number.BasisPoint;
import com.moneychange.number.Rate;
import com.moneychange.transaction.Transaction;
import com.moneychange.transaction.TransactionResult;

import java.math.BigDecimal;

public class CurrencyExchangeService {

    private MarkUpTable markUpTable;


    public CurrencyExchangeService() {
        this.markUpTable = MarkUpProvider.getMarkupTable();
    }

    public TransactionResult processTransaction(Transaction transaction){

        CurrencyAmount baseAmount = CurrencyAmount.builder()
                                        .currency(transaction.getBaseCCY())
                                        .amount(transaction.getBaseAmount())
                                        .time(transaction.getTime())
                                        .build();

        BasisPoint markUpBasisPoint = markUpTable.getEligibleMarkUp(transaction.getClientType(),baseAmount).getBasisPoint();
        BigDecimal calculatedBps =  BigDecimal.ONE.subtract((markUpBasisPoint.val().divide(new BigDecimal(100))).divide(new BigDecimal(100)));
        CurrencyAmount targetExchangeAmount = baseAmount.exchangeAmount(transaction.getTargetCCY(),transaction.getTime());

        Rate finalRate =  new Rate(targetExchangeAmount.getExchangeRate().val().multiply(calculatedBps));
        Rate profileRate = new Rate(targetExchangeAmount.getExchangeRate().val().subtract(finalRate.val()));
        Amount profit = new Amount(profileRate.val().multiply(transaction.getBaseAmount().val()));

        Amount profitInSGD = (CurrencyAmount.builder()
                .amount(profit)
                .currency(targetExchangeAmount.getCurrency())
                .time(targetExchangeAmount.getTime())
                .build())
                .exchangeAmount("SGD",targetExchangeAmount.getTime())
                .getAmount();

        return TransactionResult.builder()
                .baseCurrency(transaction.getBaseCCY())
                .wantedCurrency(transaction.getTargetCCY())
                .amountInBaseCurrency(transaction.getBaseAmount())
                .profitInWantedCurrency(profit)
                .standardRate(targetExchangeAmount.getExchangeRate())
                .profitInSGD(profitInSGD)
                .finalRate(finalRate).build();

    }

}
