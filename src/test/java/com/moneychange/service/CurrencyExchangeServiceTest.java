package com.moneychange.service;
import com.moneychange.mapper.CSVMapper;
import com.moneychange.number.Amount;
import com.moneychange.transaction.Transaction;
import com.moneychange.transaction.TransactionResult;
import junit.framework.TestCase;

import java.time.LocalTime;
import java.util.List;

public class CurrencyExchangeServiceTest extends TestCase {

    public void testProcessTransaction() {
        CurrencyExchangeService currencyExchangeService = new CurrencyExchangeService();
        TransactionResult result = currencyExchangeService.processTransaction(
                Transaction.builder()
                        .baseCCY("CNY")
                        .targetCCY("SGD")
                        .baseAmount(new Amount("40000"))
                        .clientType("Individual")
                        .time(LocalTime.of(8,32)).build());

        System.out.println(result.toString());
    }

    public void testProcessTransactionCorporate() {
        CurrencyExchangeService currencyExchangeService = new CurrencyExchangeService();
        TransactionResult result = currencyExchangeService.processTransaction(
                Transaction.builder()
                        .baseCCY("AUD")
                        .targetCCY("SGD")
                        .baseAmount(new Amount("3500000"))
                        .clientType("Corporate")
                        .time(LocalTime.of(9,43)).build());

        System.out.println(result.toString());
    }

    public void testProcessTransactions(){
        List<Transaction> transactionList = CSVMapper.loadTransactions().get();
        CurrencyExchangeService currencyExchangeService = new CurrencyExchangeService();
        transactionList.forEach(transaction -> {
            System.out.println(currencyExchangeService.processTransaction(transaction).toString());
        });
    }

}