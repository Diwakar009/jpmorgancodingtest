package com.moneychange.mapper;

import com.moneychange.markup.MarkUp;
import com.moneychange.rate.ExchangeRate;
import com.moneychange.transaction.Transaction;
import junit.framework.TestCase;
import org.junit.Assert;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class CSVMapperTest extends TestCase {

    public void testLoadMarkUpTable(){
        Map<String, List<MarkUp>> mapper =  CSVMapper.loadMarkUpTable();
        Assert.assertEquals(2,mapper.size());
    }

    public void testLoadRates() {
        Optional<List<ExchangeRate>> rateList = CSVMapper.loadRates();
        Assert.assertEquals(28,rateList.get().size());
    }

    public void testRatesUsingTime(){
        Optional<List<ExchangeRate>> rateList = CSVMapper.loadRates();

        Map<String, List<ExchangeRate>> rateMap = rateList.get().stream()
                .collect(Collectors.groupingBy(rate -> rate.getBaseCurrency() + "|" + rate.getWantedCurrency()
                        , Collectors.mapping(rate -> rate, Collectors.toList())));

        rateMap.forEach((key,values) -> System.out.println("Key: " + key + ", Values: " + values));


    }

    public void testLoadTransactions(){
        Optional<List<Transaction>> transactions = CSVMapper.loadTransactions();
        Assert.assertEquals(5,transactions.get().size());
    }


}