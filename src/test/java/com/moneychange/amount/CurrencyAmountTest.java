package com.moneychange.amount;

import com.moneychange.number.Amount;
import junit.framework.TestCase;
import org.junit.Assert;

import java.math.BigDecimal;
import java.time.LocalTime;

public class CurrencyAmountTest extends TestCase {

    public void testExchangeAmount() {
        String baseCurrency = "CNY";
        String wantedCurrency = "USD";
        CurrencyAmount amount = CurrencyAmount.builder()
                .currency(baseCurrency)
                .amount(new Amount("40000")).build();

        CurrencyAmount exchangeAmt = amount.exchangeAmount(wantedCurrency, LocalTime.of(8,01));
        Assert.assertEquals(new BigDecimal("6440.00"),exchangeAmt.getAmount().val());
        Assert.assertEquals("USD",exchangeAmt.getCurrency());
        Assert.assertEquals(new BigDecimal("0.1610"),exchangeAmt.getExchangeRate().val());
    }
}