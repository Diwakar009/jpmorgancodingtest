package com.moneychange.markup;

import junit.framework.TestCase;
import org.junit.Assert;

public class MarkUpProviderTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
    }

    public void testGetIndivisualMarkupTable() {
        MarkUpTable markupTable = MarkUpProvider.getMarkupTable();
        Assert.assertEquals(4,markupTable.getTable().get("Individual").size());
    }

    public void testGetCorporateMarkupTable() {
        MarkUpTable markupTable = MarkUpProvider.getMarkupTable();
        Assert.assertEquals(3,markupTable.getTable().get("Corporate").size());
    }

}