package com.moneychange.markup;

import com.moneychange.amount.CurrencyAmount;
import com.moneychange.number.Amount;
import junit.framework.TestCase;
import org.junit.Assert;

import java.math.BigDecimal;
import java.time.LocalTime;

public class MarkUpTableTest extends TestCase {

    public void testGetIndividualEligibleMarkUp() {
        MarkUpTable markUpTable = MarkUpProvider.getMarkupTable();
        MarkUp markUp = markUpTable.getEligibleMarkUp("Individual"
                    , CurrencyAmount.builder()
                                .currency("CNY")
                        .amount(new Amount("40000"))
                        .time(LocalTime.of(8,01)).build());
        Assert.assertEquals(new BigDecimal("40"),markUp.getBasisPoint().val());
    }

    public void testGetCorporateEligibleMarkUp() {
        MarkUpTable markUpTable = MarkUpProvider.getMarkupTable();
        MarkUp markUp = markUpTable.getEligibleMarkUp("Corporate",
                CurrencyAmount.builder().amount(new Amount("3500000"))
                        .currency("AUD")
                        .time(LocalTime.of(9,43)).build());

        Assert.assertEquals(new BigDecimal(5),markUp.getBasisPoint().val());
    }
}