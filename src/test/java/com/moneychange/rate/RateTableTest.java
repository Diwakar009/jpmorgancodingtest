package com.moneychange.rate;

import junit.framework.TestCase;
import org.junit.Assert;

import java.math.BigDecimal;
import java.time.LocalTime;

public class RateTableTest extends TestCase {

    public void testGetExchangeRate() {
        RateTable rateTable = RateProvider.getRateTable();
        ExchangeRate rate = rateTable.getExchangeRate("CNY","SGD", LocalTime.of(8,32));
        Assert.assertEquals(new BigDecimal("0.2012"),rate.getRate().val());
    }

    public void testGetUSDExchangeRate() {
        RateTable rateTable = RateProvider.getRateTable();
        ExchangeRate rate = rateTable.getUSDExchangeRate("CNY", LocalTime.of(8,32));
        Assert.assertEquals(new BigDecimal("0.1610"),rate.getRate().val());
    }
}